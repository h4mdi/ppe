import React, {Component} from 'react';
import './CSS/CoursePage.css'
import {NavLink,Redirect} from 'react-router-dom';
import CourseDesc from './CourseDesc';
import CourseVideo from './CourseVideo';
import axios from '../../ApiServices/axiosUrl';
import VideoList from './VideoList';
import Layout from '../../components/Layout/Layout';
import parse from 'html-react-parser';
import ProgressBar from 'react-bootstrap/ProgressBar';
import AuthServices from '../../ApiServices/auth.service';
import Rating from './Rating';
import CategorieService from "../../ApiServices/CategorieService";

class CoursePage extends Component {

    constructor(props) {

        super(props)
        this.state= {CoursesInfo:[]
        }

    }



    componentDidMount(){

        AuthServices.FetchCourses(this.props.match.params.Courseid)
        .then(response => {
            console.log("CoursePage Response",response);

            this.setState({CoursesInfo: response.data});

            let count=0;

        for(let j in response.data.course.videoContent){
            for (let i in response.data.course.videoContent[j].usersWatched){
                if(localStorage.getItem('userId')===response.data.course.videoContent[j].usersWatched[i]){
                    this.setState({['video'+j+'Completed']:true});
                    count+=1;
                    break;
                }
            }
        }

      let progress = (count/this.state.CoursesInfo.videoUrl.length)*100;
      this.setState({WatchedVideoCount:count,progress:progress})

        })
        .catch(error => {
            console.log(error.response);
        })
    }
    
    VideochangeHandler=(event,video,index,playing)=> {
       let VideoNumber = 'video' + index;
       this.setState({CurrentVideo:video})
       this.setState({index:index})
     
      for(let i=0;i<5;i++){
        if(i===index){
            this.setState({[VideoNumber]:true})
        }
        else{
            this.setState({['video'+i]:false})
        }
      }
     

       
        if(playing){
            this.setState({playing:true})
        }
        else{
            this.setState({playing:false})
        }
   
    }


    videoCompleted=(index)=> {
     
       if(!this.state['video'+index+'Completed']) {
       this.setState(prevState => 
        ({WatchedVideoCount:prevState.WatchedVideoCount+1}));


        const form = {}; 
        form['courseId']= this.state.CourseId;
        //form['userId']=localStorage.getItem('userId');
        //form['videoId']=this.state.CoursesInfo.videoContent[index]._id;
        console.log(form['videoId'])
           axios.post('/watchedByuser',form)
           
           .then(response => {
            console.log("Video information sent Response",response);
        })
    
        .catch(error => {
            console.log(error.response);
        })
       }

   
       
       let progress = (this.state.WatchedVideoCount/this.state.CoursesInfo.videoUrl.length)*100;
       this.setState({progress:progress})
       this.setState({['video'+index+'Completed']:true});
      
    }

    videoDuration =(duration,index)=>{
        this.setState({['video'+index+'Duration']:duration})
    }  

    render(){


        if(this.state.redirect)
        return <Redirect to={this.state.redirect}/>;


        let title = null;
        let short_description=null;
        let teacher=null;
        let createdAt=null;
        let VideoUrl=null;
        let rating=parseInt('0');
        let bookmark=false;
        let ratingtimesUpdated=null;
        let requirement=null;
        let longDescription=null;
        let willLearn=null;
        let videourl=null;
        let CurrentVideo="";
        let playButton='';
        let playingVideo=false;
        let completed=false;
        let progressbar=null;


        console.log("rf", this.state.CoursesInfo) ;

        if(this.state.loading ===false){
            console.log("rf", this.state.CoursesInfo) ;

                title = (this.state.CoursesInfo.lessonTitle);
                console.log("rf",title) ;
                short_description = (this.state.CoursesInfo.description);
                teacher=(this.state.CoursesInfo.name)
                createdAt=(this.state.CoursesInfo.lastUpdated);
                //videoUrl=(this.state.CoursesInfo.picture);
                rating=(this.state.CoursesInfo.rating.ratingFinal);
                requirement=parse(this.state.CoursesInfo.requirement);
                longDescription=parse(this.state.CoursesInfo.description);
                willLearn=parse(this.state.CoursesInfo.willLearn);
                ratingtimesUpdated=(this.state.CoursesInfo.rating.timesUpdated);
                videourl=(this.state.CoursesInfo.videoUrl.slice(0));
                CurrentVideo =this.state.CoursesInfo.videoUrl;
                bookmark = (this.state.CoursesInfo.bookmark.includes(localStorage.getItem('userId'))) 
                
                if(rating ===0) rating=1;
                
                VideoUrl= (
                    videourl.map((video,index)=>{
                    let VideoNumber ='video'+index;
                    if(this.state[VideoNumber]){
                        playButton='VideoSelected';
                        playingVideo=true;
                    }
                    else{
                        playButton='VideoNotSelected';
                        playingVideo=false;
                    }

                    if(this.state['video'+index+'Completed']){
                        completed='VideoCompleted';
                    }

                    else if(!this.state['video'+index+'Completed']){
                        completed=false;
                    }
                
               return(

                    <VideoList
                        key={index}
                        video={video}
                        
                        changed={(event)=> this.VideochangeHandler(event,video,index,playingVideo)}
                        playButton={playButton}
                        completed={completed}
                        
                        title={'Video '+ index}
                        Duration={this.state['video'+index+'Duration']}
                    />)
            
                
                     } )
                );


        }
        
        if(this.state.progress===100){
        progressbar = <p>Congratulations {localStorage.getItem('userName')}!  
          <i className="fa fa-birthday-cake" style={{marginLeft:'5px'}} aria-hidden="true"></i></p>
        }

        else{
            progressbar=(<>
             <p>You have Completed <b>{this.state.progress}% </b> of your course!</p>
                            <ProgressBar variant="success" now={this.state.progress} />
            </>);
        }

        return(



          <Layout >
            <div className="coursePage">


            <div className="container">
                                
                <nav aria-label="breadcrumb">

                        <ol className="breadcrumb">
                            <li className="breadcrumb-item">
                                <NavLink to='/home'>
                                    Home
                                </NavLink></li>

                            <li className="breadcrumb-item">
                                <NavLink to={`/Home/${this.state.CourseType}`}

                                >
                                    {this.state.CourseType}
                                </NavLink>
                            </li>


                            <li className="breadcrumb-item">
                                <NavLink to={`/course/${this.state.CourseType}/${this.state.CourseId}`}

                                activeStyle={{textDecoration:'underline'}}>
                                    {title}
                                </NavLink>
                            </li>

                        </ol>
                
                </nav>

                    <div className="Main-Section">
                    
                        <div className="Description-main">

                            <CourseDesc
                                        title={this.state.CoursesInfo.lessonTitle}
                                        short_description={this.state.CoursesInfo.description}
                                        createdat={this.state.CoursesInfo.lastUpdated}
                                        CourseId={this.state.lessonId}
                                        rating={parseInt("2")}
                                        CourseType={this.state.CourseType}
                                        bookmark={bookmark}
                         
                            />

                        </div>

                            <div className="Course-Video">
               

                                <CourseVideo playing={CurrentVideo}
                                    videoUrl={this.state.CoursesInfo.videourl}
                                    index={this.state.index}
                                    videoCompleted={this.videoCompleted}
                                    videoDuration={this.videoDuration}

                                    
                                              />
                            </div>


                     </div>


            <div className="Breakpoint"></div>

           <div className="Section2">
                
                <div className="section2part1">
                
                        <div className="Small-nav-section">

                            <p >About</p>
                            {/* <p>Instructor</p>
                            <p>About</p> */}


                        </div>


                            
                        <div className="flex-col-requirement">
                            
                            <h1>Requirement of this Course</h1>
                            <p>{this.state.CoursesInfo.content}</p>
                    
                        </div>

                            
                        <div className="flex-col-requirement">
                            
                            <h1>Descripton</h1>
                            <p>{ this.state.CoursesInfo.description}</p>
                    
                        </div>

                        <div className="flex-col-requirement">
                            
                            <h1>What will you learn from this course?</h1>
                            <p>{this.state.CoursesInfo.content}</p>
                    
                        </div>


                 </div>

                    <div style={{marginBottom:"100px"}} className="flex-center">
        
                        {VideoUrl}
                        <div className='progressBar'>
                            
                           {progressbar}

                        </div>

                        <div className="progressBar">
                            <p className="Rating_coursePage">Rate the course here please</p>
                            <Rating style={{justifyContent:'center'}}
                                rating={parseInt(rating)}
                                edit={true}
                                specialrating={true} 
                                CourseId={this.state.courseId}/>
                        </div>
                     </div>

                    

            </div>

       
            


        </div>
        </div>
        </Layout>

        );
    }

}

export default CoursePage;