import React, { Component } from 'react';
import { withStyles } from "@material-ui/core/styles"
import http from '../../api/http'
import CourseVideo from "../CoursePage/CourseVideo";
import VideoList from "../CoursePage/VideoList";
import Layout from "../Layout/Layout";

const styles = theme => ({

})


class CourseHome extends Component {
    constructor(props){
        super(props)

        this.state = {
                    courseName: "Welcome to the dummy course",
                    description: "Here are some resources for the course",
                    instructor: {
                        "firstName": "Teacher",
                        "lastName": "User",
                        "email": "teacher@cc.net",
                    },
                    buildingName:"Wber Engineering",
                    roomNumber:"410",
            courseChaptersiD:"0"
                }
        this.getCourseAssignments(sessionStorage.getItem("courseId"));

    }

    getChaptersByCourse(courseId){
        http.getChaptersByCourse(courseId)
            .then( async(response) => {
                const body = await response.json();
                if(response.status === 200 && body["message"] === "success"){
                    var simpleChapters=[]
                    for (var i in body["chapters"]) {

                        simpleChapters[i] = {
                            title: body["chapters"][i]["lessonNo"],
                            id: body["chapters"][i]["id"],
                            image: '',
                        }


                    }


                    this.setState({ chapterz:simpleChapters });
                }
            })
            .catch((e) => {
                console.warn("There was an error retrieving instructor courses: ", e);

                this.setState({
                    error: "There was an error retrieving instructor courses."
                });
            });
    }




    getCourseAssignments(courseId){
        http.getCourseDetails(courseId)
            .then( async(response) => {
            var body = await response.json();
            if(response.status === 200 && body["message"] === "success"){
                this.setState({
                    courseName: body["course"]["courseName"],
                    description: body["course"]["description"],
                    instructor: body["course"]["instructor"],
                    buildingName: body["course"]["buildingName"],
                    roomNumber: body["course"]["roomNumber"],
                    courseChaptersiD: body["course"]["courseChaptersiD"],


                })
            }
        })

        http.getChaptersByCourse(courseId)
            .then( async(response) => {
                const body = await response.json();
                if(response.status === 200 && body["message"] === "success"){
                    var simpleChapters=[]
                    for (var i in body["chapters"]) {

                        simpleChapters[i] = {
                            title: body["chapters"][i]["lessonNo"],
                            id: body["chapters"][i]["id"],
                            image: '',
                        }


                    }


                    this.setState({ chapterz:simpleChapters });
                }
            })
        .catch((e) => {
            console.warn("There was an error retrieving instructor courses: ", e);
    
            this.setState({
                error: "There was an error retrieving instructor courses."
            });
        });
    }
    

    render() {
        return (

            <Layout >
                <div className="coursePage">
                    <ul>
                    <h1>Course: {this.state.courseName}</h1>
                    <p>Description: {this.state.description}</p>
                    <p>Instructor: {this.state.instructor.firstName} {this.state.instructor.lastName} - {this.state.instructor.email}</p>
                    <p>Location: {this.state.buildingName} {this.state.roomNumber} </p>
                    <p>Location: {this.state.courseChaptersiD} </p>



                </ul>


                <div className="Course-Video">


                    <CourseVideo playing="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"
                                 videoUrl="https://media.w3.org/2010/05/sintel/trailer_hd.mp4"


                    />
                </div>


                <div>
                    <h1>{this.state.title}</h1>
                </div>
                </div>
                <div className="Breakpoint"></div>

                <div className="Section2">

                    <div className="section2part1">

                        <div className="Small-nav-section">

                            <p >About</p>
                            {/* <p>Instructor</p>
                            <p>About</p> */}


                        </div>



                        <div className="flex-col-requirement">

                            <h1>Requirement of this Course</h1>
                            <p></p>

                        </div>


                        <div className="flex-col-requirement">

                            <h1>Descripton</h1>
                            <p></p>

                        </div>

                        <div className="flex-col-requirement">

                            <h1>What will you learn from this course?</h1>
                            <p></p>

                        </div>


                    </div>

                    <div style={{marginBottom:"100px"}} className="flex-center">


                        <div className='progressBar'>


                        </div>

                        <div className="progressBar">
                            <p className="Rating_coursePage">Rate the course here please</p>

                        </div>
                    </div>



                </div>
            </Layout>



        )




    }


}

export default withStyles(styles)(CourseHome)