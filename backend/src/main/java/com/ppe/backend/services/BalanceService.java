package com.ppe.backend.services;

import com.ppe.backend.models.Balance;
import com.ppe.backend.models.User;
import com.ppe.backend.repositories.BalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BalanceService {

    @Autowired
    private BalanceRepository balanceRepository;

    public void saveBalance(Balance balance) {
        balanceRepository.save(balance);
    }
    public List<Balance> getBalance(User user){return balanceRepository.findByUser(user);}
}