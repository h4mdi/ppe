package com.ppe.backend.repositories;

import com.ppe.backend.models.Balance;
import com.ppe.backend.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BalanceRepository extends MongoRepository<Balance, String> {
    List<Balance> findByUser(User user);
}
