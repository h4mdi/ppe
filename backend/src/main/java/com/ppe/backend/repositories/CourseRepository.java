package com.ppe.backend.repositories;

import com.ppe.backend.models.Course;
import com.ppe.backend.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends MongoRepository<Course, String> {
    List<Course> findByInstructor(User instructor);
}
