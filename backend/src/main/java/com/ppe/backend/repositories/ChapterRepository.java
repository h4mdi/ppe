package com.ppe.backend.repositories;

import com.ppe.backend.models.Chapter;
import com.ppe.backend.models.Course;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChapterRepository extends MongoRepository<Chapter, Long> {
    List<Chapter> findByCourse(Course course);
}
