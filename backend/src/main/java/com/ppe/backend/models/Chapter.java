package com.ppe.backend.models;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

@NoArgsConstructor
@AllArgsConstructor
@Data
@RequiredArgsConstructor

public class Chapter {

    @Id
    @NonNull
    @Getter @Setter
    private Long lessonNo;


    @DBRef(lazy = true)
    private Course course;

    @NonNull
    @Getter @Setter
    private String thumburl;
    @NonNull
    @Getter @Setter
    private String videourl;

    @NonNull
    @Getter @Setter
    private String level;


}
